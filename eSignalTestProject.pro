include(common.pri)
include(app.pri)

QT       += core gui widgets

TARGET = eSignalTestProject
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += src/main.cpp\
        src/ui/MainWindow.cpp \
    src/processing/DrivesWatcher.cpp \
    src/processing/PathInfoWorker.cpp \
    src/processing/PathInfoCollector.cpp \
    src/data/ExtensionInfo.cpp \
    src/ui/InfoWidget.cpp

HEADERS  += src/ui/MainWindow.h \
    src/processing/DrivesWatcher.h \
    src/processing/PathInfoWorker.h \
    src/processing/PathInfoCollector.h \
    src/data/ExtensionInfo.h \
    src/ui/InfoWidget.h

FORMS    += src/ui/MainWindow.ui \
    src/ui/InfoWidget.ui
