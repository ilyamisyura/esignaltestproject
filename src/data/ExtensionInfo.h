#ifndef EXTENTIONINFO_H
#define EXTENTIONINFO_H

#include <QString>
#include <QFileInfo>
#include <QMetaType>

/**
 * @brief The ExtentionInfo class provides collected information
 * about all files with some extention in directory including subdirectories
 */
class ExtensionInfo
{

    quint32 m_filesCount;
    quint64 m_filesSize; //bytes
    QString m_extension;

public:
    ExtensionInfo();
    /**
     * @todo make a copy constructor
     */

    void increaseCount();
    void addCount(quint32 count);
    void setExtension(QString extension);
    void increaseSize(quint64 size);
    void addFileInfo(QFileInfo fileInfo);

    quint32 count() { return m_filesCount; }
    quint32 filesSize() { return m_filesSize; }
    QString extension() { return m_extension; }
    quint32 averageSize();
};

typedef QMap<QString,ExtensionInfo*> ExtensionInfoMap;
Q_DECLARE_METATYPE(ExtensionInfo)

#endif // EXTENTIONINFO_H
