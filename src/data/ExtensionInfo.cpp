#include "ExtensionInfo.h"
#include <QDebug>
#include <QMutex>

ExtensionInfo::ExtensionInfo()
{
    m_filesCount = 0;
    m_extension = "";
    m_filesSize = 0;
}

void ExtensionInfo::addCount(quint32 count)
{
    m_filesCount+=count;
}

void ExtensionInfo::increaseCount()
{
    m_filesCount++;
}

void ExtensionInfo::increaseSize(quint64 size)
{
    QMutex mutex;
    mutex.lock();
    m_filesSize+=size;
    mutex.unlock();
}

void ExtensionInfo::setExtension(QString extension)
{
    m_extension = extension;
}

void ExtensionInfo::addFileInfo(QFileInfo fileInfo)
{
    increaseCount();
//    if (fileInfo.suffix().toLower() == "wav")
//    {
//        qDebug() << "wav old" << m_filesSize;
//    }
    increaseSize(fileInfo.size());
//    if (fileInfo.suffix().toLower() == "wav")
//    {
//        qDebug() << "wav new" << m_filesSize;
//    }
}

quint32 ExtensionInfo::averageSize()
{
    quint32 averageSize;
    averageSize = m_filesSize/m_filesCount;
    return averageSize;
}
