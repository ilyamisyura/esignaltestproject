#include "PathInfoCollector.h"
#include <QDirIterator>
#include <QDebug>
#include <QElapsedTimer>

const QString PathInfoCollector::KEY_NO_EXTENSION = "No extension";
const QString PathInfoCollector::KEY_ALL_FILES = "All files";

PathInfoCollector::PathInfoCollector(QObject *parent) : QObject(parent)
{
    m_path = "";
    m_filesCount = 0;
    m_overallSize = 0;
}

void PathInfoCollector::setPath(QString path)
{
    QDir dir(path);
    if (!dir.exists())
    {
        qDebug() << __PRETTY_FUNCTION__ << "No such directory" << m_path;
        return;
    }
    else
    {
        m_path = path;
    }
}

void PathInfoCollector::collectInfo()
{
    if (m_path.isEmpty())
    {
        qDebug() << __PRETTY_FUNCTION__ << "Directory is not set";
        return;
    }
    QElapsedTimer timer;
    timer.start();
    QDirIterator di(m_path, QDir::Files, QDirIterator::Subdirectories);
    QFileInfo currentFileInfo;
    while (di.hasNext())
    {
        currentFileInfo = QFileInfo(di.next());
        if (currentFileInfo.isFile())
        {
            m_overallSize+= currentFileInfo.size();
            m_filesCount++;
            collectFileInfo(currentFileInfo);
            emit infoChanged();
        }
    }

    emit finished();
}


void PathInfoCollector::collectFileInfo(QFileInfo fileInfo)
{
    QString suffix = fileInfo.suffix().toLower();
    if (suffix.isEmpty())
    {
        suffix = KEY_NO_EXTENSION;
    }

    addFileInfoToMap(fileInfo,suffix);
//    addFileInfoToMap(fileInfo,KEY_ALL_FILES);
}

void PathInfoCollector::addFileInfoToMap(QFileInfo fileInfo, QString suffix)
{
    if (m_fileExtensions.contains(suffix))
    {
        m_fileExtensions.value(suffix)->addFileInfo(fileInfo);
    }
    else
    {
        ExtensionInfo* newExtensionInfo = new ExtensionInfo;
        newExtensionInfo->setExtension(suffix);
        newExtensionInfo->addFileInfo(fileInfo);
        m_fileExtensions.insert(suffix, newExtensionInfo);
    }
}

ExtensionInfo* PathInfoCollector::getExtensionInfo(QString extension)
{
    ExtensionInfo* result = NULL;
    if (m_fileExtensions.contains(extension))
    {
        result = m_fileExtensions.value(extension);
    }
    else
    {
        qDebug() << "No files with extension" << extension << "in chosen folder";
    }
    return result;
}

void PathInfoCollector::clearInfo()
{
    m_filesCount = 0;
    m_overallSize = 0;
    foreach (QString key, m_fileExtensions.keys())
    {
        delete m_fileExtensions.value(key);
    }
    m_fileExtensions.clear();
}
