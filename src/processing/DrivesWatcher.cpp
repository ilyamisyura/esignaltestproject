#include "DrivesWatcher.h"
#include <QDebug>

DrivesWatcher::DrivesWatcher(QObject *parent) : QObject(parent)
{
    m_pThread = new QThread(this);
    m_pTimer = new QTimer(this);
    m_pTimer->setInterval(2000);
    QObject::connect(m_pTimer,&QTimer::timeout,this,&DrivesWatcher::checkDrives);
    QObject::connect(m_pThread,SIGNAL(started()),m_pTimer,SLOT(start()));
    QObject::connect(m_pThread,SIGNAL(finished()),m_pTimer,SLOT(stop()));

    m_currentMountedVolumes = QStorageInfo::mountedVolumes();
}

void DrivesWatcher::run()
{
    m_pThread->start();
}

void DrivesWatcher::stop()
{
    m_pThread->quit();
}

void DrivesWatcher::checkDrives()
{
    QList<QStorageInfo> previousContent = m_currentMountedVolumes;
    m_currentMountedVolumes = QStorageInfo::mountedVolumes();

    quint32 prevSize = previousContent.size();
    quint32 currSize = m_currentMountedVolumes.size();

    for (quint32 i=0; i<prevSize; ++i)
    {
        QStorageInfo si = previousContent.at(i);
        if (!m_currentMountedVolumes.contains(si))
        {
            emit s_deviceDisconnected(si);
            qDebug() << "Disconnected drive" << si.displayName() << si.rootPath();
        }
    }

    for (quint32 i=0; i<currSize; ++i)
    {
        QStorageInfo si = m_currentMountedVolumes.at(i);
        if (!previousContent.contains(si))
        {
            emit s_deviceConnected(si);
            qDebug() << "Connected drive" << si.displayName() << si.rootPath();
        }
    }
}
