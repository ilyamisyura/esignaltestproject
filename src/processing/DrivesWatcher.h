#ifndef DRIVESWATCHER_H
#define DRIVESWATCHER_H

#include <QObject>
#include <QStorageInfo>
#include <QThread>
#include <QTimer>


class DrivesWatcher : public QObject
{
    Q_OBJECT

    QThread* m_pThread;
    QTimer* m_pTimer;
    QList<QStorageInfo> m_currentMountedVolumes;

public:
    explicit DrivesWatcher(QObject *parent = 0);

signals:
    void s_deviceConnected(QStorageInfo drive);
    void s_deviceDisconnected(QStorageInfo drive);

private slots:
    void checkDrives();

public slots:
    void run();
    void stop();
};

#endif // DRIVESWATCHER_H
