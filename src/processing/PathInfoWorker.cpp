#include "PathInfoWorker.h"

PathInfoWorker::PathInfoWorker(QObject *parent) : QObject(parent)
{
    m_pPathInfoCollector = new PathInfoCollector();
    m_pThread = new QThread(this);
    QObject::connect(m_pThread,&QThread::started,m_pPathInfoCollector,&PathInfoCollector::collectInfo);
    QObject::connect(m_pPathInfoCollector,&PathInfoCollector::finished,this,&PathInfoWorker::stopCollectingInfo);
    m_pPathInfoCollector->moveToThread(m_pThread);
}

void PathInfoWorker::setPath(QString path)
{
    m_path = path;
    m_pPathInfoCollector->setPath(m_path);
}

void PathInfoWorker::startCollectingInfo()
{
    m_pThread->start();
    m_isRunning = true;
    emit startedCollectingInfo(m_path);
}

void PathInfoWorker::stopCollectingInfo()
{
    m_pThread->quit();
    m_isRunning = false;
    emit finishedCollectingInfo(m_path);
}
