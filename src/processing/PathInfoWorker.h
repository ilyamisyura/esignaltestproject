#ifndef PATHINFOWORKER_H
#define PATHINFOWORKER_H

#include <QObject>
#include <QThread>
#include "src/processing/PathInfoCollector.h"

class PathInfoWorker : public QObject
{
    Q_OBJECT

    QThread* m_pThread;
    PathInfoCollector* m_pPathInfoCollector;
    QString m_path;

    bool m_isRunning;

public:
    explicit PathInfoWorker(QObject *parent = 0);
    void setPath(QString path);
    QString path() { return m_path; }
    bool isRunning() { return m_isRunning; }
    PathInfoCollector* pathInfoCollector() { return m_pPathInfoCollector; }

signals:
    void startedCollectingInfo(QString path);
    void finishedCollectingInfo(QString path);

public slots:
    void startCollectingInfo();
    void stopCollectingInfo();
};

#endif // PATHINFOWORKER_H
