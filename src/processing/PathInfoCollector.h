#ifndef PATHINFOCOLLECTOR_H
#define PATHINFOCOLLECTOR_H

#include <QObject>
#include <QStringList>
#include <QMap>
#include <QFileInfo>
#include <QDirIterator>
#include "src/data/ExtensionInfo.h"

class PathInfoCollector : public QObject
{
    Q_OBJECT

    ExtensionInfoMap m_fileExtensions;
    static const QString KEY_NO_EXTENSION;
    static const QString KEY_ALL_FILES;
    quint64 m_overallSize;
    quint64 m_filesCount;
    QString m_path;
    QString m_currentPath;

    void collectFileInfo(QFileInfo fileInfo);
    void addFileInfoToMap(QFileInfo fileInfo, QString key);

public:
    explicit PathInfoCollector(QObject *parent = 0);

    void setPath(QString path);

    ExtensionInfoMap getExtensionsInfo() { return m_fileExtensions; }
    quint64 overallSize() { return m_overallSize; }
    quint64 filesCount() { return m_filesCount; }
    ExtensionInfo* getExtensionInfo(QString extension);

signals:
    void infoChanged();
    void finished();

public slots:
    void collectInfo();
    void clearInfo();
};

#endif // PATHINFOCOLLECTOR_H
