#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemWatcher>
#include <QFileSystemModel>
#include <QStorageInfo>
#include "src/processing/DrivesWatcher.h"
#include "src/processing/PathInfoWorker.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    DrivesWatcher* p_watcher;
    QFileSystemModel* p_fileSystemModel;
    QString m_currentPath;
    QMap<QString,PathInfoWorker*> m_pathInfoWorkers;

    void setUpConnections();

    QString getCurrentSelectedPath();

private slots:
    void addDrive(QStorageInfo drive);
    void deleteDrive(QStorageInfo drive);
    void updateCurrentDrivesInfo();
    void on_drivesComboBox_currentIndexChanged(int index);

    void checkPathInfo(const QModelIndex& index);
    void getPathInfoFromModel(const QModelIndex& index);
    void showPathInfo(QString path);
    void clearInfo();
    void showExtensionInfo(QString extension);
    void on_rescanPushButton_clicked();
    void on_clearLogPushButton_clicked();
};

#endif // MAINWINDOW_H
