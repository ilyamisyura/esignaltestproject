#include "InfoWidget.h"
#include "ui_InfoWidget.h"

InfoWidget::InfoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InfoWidget)
{
    ui->setupUi(this);

    setUpConnections();

    reset();
}

void InfoWidget::setUpConnections()
{
    QObject::connect(this,SIGNAL(overallSizeChanged()),SLOT(redrawOverallSize()));
    QObject::connect(this,SIGNAL(filesCountChanged()),SLOT(redrawFilesCount()));
}

InfoWidget::~InfoWidget()
{
    delete ui;
}

void InfoWidget::setOverallSize(quint64 overallSizeBytes)
{
    m_overallSize = overallSizeBytes;
    emit overallSizeChanged();
}

void InfoWidget::setFilesCount(quint64 filesCount)
{
    m_filesCount = filesCount;
    emit filesCountChanged();
}

void InfoWidget::redrawOverallSize()
{
    QString overallSizeString = QString::number(m_overallSize);
    formatSizeString(overallSizeString);
    ui->overallSizeValueLabel->setText(overallSizeString.append(" bytes"));
    redrawAverageSize();
}

void InfoWidget::redrawFilesCount()
{
    ui->filesCountValueLabel->setText(QString::number(m_filesCount));
    redrawAverageSize();
}

void InfoWidget::redrawAverageSize()
{
    if (m_filesCount == 0)
    {
        ui->averageSizeValueLabel->clear();
        return;
    }

    m_averageSize = m_overallSize / m_filesCount;
    QString averageSizeString = QString::number(m_averageSize);
    formatSizeString(averageSizeString);
    ui->averageSizeValueLabel->setText(averageSizeString.append(" bytes"));
}

void InfoWidget::reset()
{
    m_overallSize = 0;
    m_filesCount = 0;
    m_averageSize = 0;
    ui->overallSizeValueLabel->clear();
    ui->filesCountValueLabel->clear();
    ui->averageSizeValueLabel->clear();
}

void InfoWidget::formatSizeString(QString &sizeString)
{
    for (int i=sizeString.size()-3; i>0; i-=3)
    {
        sizeString.insert(i,' ');
    }
}
