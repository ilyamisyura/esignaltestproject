#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "src/processing/PathInfoWorker.h"
#include <QStorageInfo>
#include <QFileSystemWatcher>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    p_watcher = new DrivesWatcher();

    p_fileSystemModel = new QFileSystemModel;
    p_fileSystemModel->setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    ui->treeView->setModel(p_fileSystemModel);
    for (int i=1; i<p_fileSystemModel->columnCount(); ++i)
    {
        ui->treeView->hideColumn(i);
    }

    setUpConnections();
    updateCurrentDrivesInfo();

    p_watcher->run();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setUpConnections()
{
    QObject::connect(p_watcher,&DrivesWatcher::s_deviceConnected,this,&MainWindow::addDrive);
    QObject::connect(p_watcher,&DrivesWatcher::s_deviceDisconnected,this,&MainWindow::deleteDrive);

    QItemSelectionModel* selectionModel = ui->treeView->selectionModel();
    QObject::connect(selectionModel,&QItemSelectionModel::currentChanged,this,&MainWindow::checkPathInfo);

    QObject::connect(p_fileSystemModel,&QFileSystemModel::directoryLoaded,[=](const QString& path)
    {
        ui->logTextEdit->append(QString("Loaded path %1\n").arg(path));
    });

    QObject::connect(ui->extensionComboBox,SIGNAL(currentIndexChanged(QString)),this,SLOT(showExtensionInfo(QString)));
}

void MainWindow::addDrive(QStorageInfo drive)
{
    ui->drivesComboBox->addItem(drive.displayName(),drive.rootPath());
}

void MainWindow::deleteDrive(QStorageInfo drive)
{
    for (int i=0; i<ui->drivesComboBox->count(); ++i)
    {
        QString itemDataStr = ui->drivesComboBox->itemData(i).toString();
        if (itemDataStr == drive.rootPath())
        {
            ui->drivesComboBox->removeItem(i);
        }
    }
}

void MainWindow::updateCurrentDrivesInfo()
{
    ui->drivesComboBox->clear();
    foreach (QStorageInfo storageInfo, QStorageInfo::mountedVolumes()) {
        ui->drivesComboBox->addItem(storageInfo.displayName(),storageInfo.rootPath());
    }
}

void MainWindow::on_drivesComboBox_currentIndexChanged(int index)
{
    Q_UNUSED(index)

    QString rootPath = ui->drivesComboBox->currentData().toString();
    p_fileSystemModel->setRootPath(rootPath);
    ui->treeView->setRootIndex(p_fileSystemModel->index(rootPath));

    clearInfo();
    ui->treeView->clearSelection();
}

void MainWindow::checkPathInfo(const QModelIndex &index)
{
    QString path = p_fileSystemModel->filePath(index);
    m_currentPath = path;
    clearInfo();

    if (m_pathInfoWorkers.contains(path))
    {
        PathInfoWorker* pathInfoWorker = m_pathInfoWorkers.value(path);
        if (pathInfoWorker->isRunning())
        {
            ui->logTextEdit->append(QString("Information about path %1 is still being collected\n").arg(path));

        }
        else
        {
            ui->logTextEdit->append(QString("Information about path %1 has been collected. Click \"Rescan\" button to refresh info\n")
                                    .arg(path));
            showPathInfo(path);
        }
    }
    else
    {
        getPathInfoFromModel(index);
    }
}

void MainWindow::showPathInfo(QString path)
{
    if (p_fileSystemModel->filePath(ui->treeView->currentIndex()) == path)
    {
        PathInfoWorker* pathInfoWorker= m_pathInfoWorkers.value(path);
        PathInfoCollector* pathInfoCollector = pathInfoWorker->pathInfoCollector();
        ui->pathInfoWidget->setOverallSize(pathInfoCollector->overallSize());
        ui->pathInfoWidget->setFilesCount(pathInfoCollector->filesCount());

        ExtensionInfoMap extensionInfoMap = pathInfoCollector->getExtensionsInfo();
        ui->extensionComboBox->clear();
        foreach (QString extension, extensionInfoMap.keys())
        {
            ui->extensionComboBox->addItem(extension,extension);
        }
    }
}

void MainWindow::showExtensionInfo(QString extension)
{
    if (!extension.isEmpty())
    {
        PathInfoWorker* pathInfoWorker= m_pathInfoWorkers.value(m_currentPath);
        PathInfoCollector* pathInfoCollector = pathInfoWorker->pathInfoCollector();
        ui->extensionInfoWidget->setOverallSize(pathInfoCollector->getExtensionInfo(extension)->filesSize());
        ui->extensionInfoWidget->setFilesCount(pathInfoCollector->getExtensionInfo(extension)->count());
    }
}

void MainWindow::clearInfo()
{
    ui->pathInfoWidget->reset();
    ui->extensionInfoWidget->reset();
    ui->extensionComboBox->clear();
}

void MainWindow::getPathInfoFromModel(const QModelIndex &index)
{
    QString path = p_fileSystemModel->filePath(index);
    PathInfoWorker* pathInfoWorker = new PathInfoWorker;
    pathInfoWorker->setPath(path);
    QObject::connect(pathInfoWorker,&PathInfoWorker::startedCollectingInfo,[=]()
    {
        ui->logTextEdit->append(QString("Started collecting info about %1 directory\n").arg(path));
    });
    QObject::connect(pathInfoWorker,&PathInfoWorker::finishedCollectingInfo,[=](QString finishedPath)
    {
        ui->logTextEdit->append(QString("Finished collecting info about %1 directory\n").arg(finishedPath));
        showPathInfo(finishedPath);
    });
    m_pathInfoWorkers.insert(path,pathInfoWorker);

    pathInfoWorker->startCollectingInfo();
}

void MainWindow::on_rescanPushButton_clicked()
{
    QString path = p_fileSystemModel->filePath(ui->treeView->currentIndex());
    if (!path.isEmpty())
    {
        m_pathInfoWorkers.value(path)->stopCollectingInfo();
        delete m_pathInfoWorkers.value(path);
        clearInfo();
        getPathInfoFromModel(ui->treeView->currentIndex());
    }
}

QString MainWindow::getCurrentSelectedPath()
{
    QString path = p_fileSystemModel->filePath(ui->treeView->currentIndex());
    return path;
}

void MainWindow::on_clearLogPushButton_clicked()
{
    ui->logTextEdit->clear();
}
