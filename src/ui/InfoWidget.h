#ifndef INFOWIDGET_H
#define INFOWIDGET_H

#include <QWidget>

namespace Ui {
class InfoWidget;
}

class InfoWidget : public QWidget
{
    Q_OBJECT

    quint64 m_overallSize;
    quint64 m_filesCount;
    quint64 m_averageSize;

public:
    explicit InfoWidget(QWidget *parent = 0);
    ~InfoWidget();

    void setOverallSize(quint64 overallSizeBytes);
    void setFilesCount(quint64 filesCount);
    void reset();

private:
    Ui::InfoWidget *ui;

    void setUpConnections();
    void formatSizeString(QString& sizeString);

signals:
    void overallSizeChanged();
    void filesCountChanged();

private slots:
    void redrawOverallSize();
    void redrawFilesCount();
    void redrawAverageSize();
};

#endif // INFOWIDGET_H
