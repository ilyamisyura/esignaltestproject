### eSignal File System Manager ###

[![N|Solid](http://www.esignal.com/medias/2015/09/logo.png)](http://www.esignal.com)

eSignal File System Manager provides a simple interface for quick directories scanning.

All you need is to select some folder, then application will run a thread which counts a number of files, its size and average file size for every file extension and for all files in global. The most innovative file surfing system!

> "I've never seen such a perfect system before!"
> Bill Gates

> "It's totally awesome!"
> My mommy

> "I can't understand a thing, but you are my hero!"
> My wife

Download and enjoy!